import React, { useEffect, useState } from 'react';
import Snackbar from "react-native-snackbar"

import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  Button,
  Linking,
} from 'react-native';
import RustoreReviewClient from 'react-native-rustore-review';

export default function App() {
  const [isLoading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    RustoreReviewClient.init();
  }, []);

  const requestReviewFLow = async () => {
    setLoading(true);
    try {
      await RustoreReviewClient.requestReviewFlow();
      showSnackBar("requestReviewFlow Success")
    } catch (err: any) {
      setError(JSON.stringify(err));
      showSnackBar("requestReviewFlow Error")
    } finally {
      setLoading(false);
    }
  };

  const launchReviewFLow = async () => {
    setLoading(true);
    try {
      await RustoreReviewClient.launchReviewFlow();
      showSnackBar("launchReviewFlow Success")
    } catch (err: any) {
      setError(JSON.stringify(err));
      showSnackBar("launchReviewFlow Error")
    } finally {
      setLoading(false);
    }
  };

  const handlePress = (async (url: string) => {
    const isSupported = await Linking.canOpenURL(url);

    if (isSupported) {
      await Linking.openURL(url);
    } else {
      showSnackBar("Don't know how to open URL (RuStore is probably not installed)")
    }
  });

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.container}>
        <Text>{error}</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.button}>
      <Button title='Request review flow' onPress={requestReviewFLow} />
      </View>
      <View style={styles.button}>
      <Button title='Launch review flow' onPress={launchReviewFLow} />
      </View>
      <View style={styles.button}>
      <Button title='Open review in RuStore' onPress={() => handlePress("rustore://apps.rustore.ru/app/ru.rustore.reviewreact")} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20
  },
  button: {
    width: 200,
    margin: 10
  }
});


const showSnackBar = (text: string) => {
  Snackbar.show({
    text: text,
    duration: Snackbar.LENGTH_SHORT,
  });
}

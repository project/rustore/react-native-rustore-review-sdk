package com.rustorereview

import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.Promise
import ru.rustore.sdk.review.RuStoreReviewManager
import ru.rustore.sdk.review.RuStoreReviewManagerFactory
import ru.rustore.sdk.review.model.ReviewInfo

class RustoreReviewModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {

  private var isInitCalled: Boolean = false

  private var reviewInfo: ReviewInfo? = null

  companion object {
    lateinit var reviewManager: RuStoreReviewManager
  }

  override fun getName(): String {
    return "RustoreReview"
  }

  @ReactMethod
  fun init() {
    if (isInitCalled) {
      return
    }
    reviewManager = RuStoreReviewManagerFactory.create(reactApplicationContext)
    isInitCalled = true
  }

  @ReactMethod
  fun requestReviewFlow(promise: Promise) {
    reviewManager.requestReviewFlow().addOnSuccessListener { reviewInfo ->
      this.reviewInfo = reviewInfo
      promise.resolve(true)
    }.addOnFailureListener { throwable ->
      promise.reject(throwable)
    }
  }

  @ReactMethod
  fun launchReviewFlow(promise: Promise) {
    val reviewInfo = this.reviewInfo
    if (reviewInfo == null) {
      val throwable = Throwable(message = "false")
      promise.reject(throwable)
      return
    }

    reviewManager.launchReviewFlow(reviewInfo).addOnSuccessListener {
      promise.resolve(true)
    }.addOnFailureListener { throwable ->
      promise.reject(throwable)
    }
  }
}

import { NativeModules } from 'react-native';

interface RustoreReviewModule {
  init: () => void;
  requestReviewFlow: () => Promise<Boolean>;
  launchReviewFlow: () => Promise<Boolean>;
}

export default NativeModules.RustoreReview as RustoreReviewModule;
